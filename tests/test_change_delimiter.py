import unittest
from csv_process.change_delimiter import replace_delimiter


class TestChangeDelimiter(unittest.TestCase):
    def test_change_delimiter(self):
        csvfile = "1,2,3"
        self.assertEqual("1*2*3", replace_delimiter(csvfile, ',', '*'))
        csvfile = "1"
        self.assertEqual(csvfile, replace_delimiter(csvfile, '|', '0'))
