import csv, os

def check_csv(path):
    if os.path.exists(path) and os.path.isfile(path) and path.split('.')[len(path.split('.')) - 1] == "csv":
        return True
    else:
        return False

def open_csv(path, delimiteur):
    pattern = {}
    datalist = []
    names = []
    with open(path) as csvfile:
        reader = csv.reader(csvfile, delimiter=delimiteur)
        slot = 0
        for row in reader:
            if len(pattern) == 0:
                for name in row:
                    pattern[name] = ""
                    names.append(name)
                datalist.append(pattern)                    
            else:
                datalist.append({})
                for indice in range(len(row)):
                    datalist[slot][names[indice]] = row[indice]
            slot = slot + 1
    return datalist

def touch(path):
    with open(path, 'a'):
        os.utime(path, None)

def write_csv(path, data, delimiteur):
    if (check_csv == False):
        touch(path)
    with open(path, 'w') as csvfile:
        writer = csv.writer(csvfile, delimiter=delimiteur)
        pattern_w = False
        
        for row in data:
            out = []
            if pattern_w == False:
                for name in row:
                    out.append(name)
                pattern_w = True
            else:
                for name in row:
                    out.append(row[name])
            
            writer.writerow(out)