import csv

with open('example.csv', 'a') as csvfile:
    writer = csv.writer(csvfile, delimiter=',')
    writer.writerow(['.', '..', '...'])
    writer.writerow(['...', '..', '.'])