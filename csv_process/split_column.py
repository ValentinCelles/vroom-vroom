import datetime

def split_column(data_in, pattern_convert):
    data_out = data_in
    
    for slot in pattern_convert.keys():
        for indice in range(len(pattern_convert[slot])):
            
            if pattern_convert[slot][indice]['action'] == "convert":
                data_out[slot] = data_out[pattern_convert[slot][indice]['data']]
            elif pattern_convert[slot][indice]['action'] == "split":
                data_out[slot] = data_out[pattern_convert[slot][indice]['data']].split(pattern_convert[slot][indice]['delimiter'])[pattern_convert[slot][indice]['index']]
            elif pattern_convert[slot][indice]['action'] == "date_format":
                data_out[slot] = date_format(data_out[pattern_convert[slot][indice]['data']], pattern_convert[slot][indice]['format_init'], pattern_convert[slot][indice]['format_final'])
    
    return data_out

def date_format(date, format_init, format_final):
    return datetime.datetime.strptime(date, format_init).strftime(format_final)
