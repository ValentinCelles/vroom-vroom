def reorder_csv(datain, pattern):
    if pattern.keys() == datain.keys():
        output = datain
    else:
        temp = datain
        for key in pattern.keys():
            if key not in temp:
                temp[key] = ""
        output = {}
        for key in pattern.keys():
            output[key] = temp[key]
    return output