import argparse
from config_pattern import pattern, pattern_convertion, initial_delimiter, final_delimiter
from csv_process.reorder_csv import reorder_csv
from csv_process.split_column import split_column
from csv_process.change_delimiter import replace_delimiter
from csv_process.file_check_open_write import check_csv, open_csv, write_csv

#Récupération des Arguments Console
parser = argparse.ArgumentParser(description='example')
parser.add_argument("file", help = ".csv file to convert")
parser.add_argument("-output", help = "Output name (Default is \"file.csv\")")
parser.add_argument("-delimiter", help = "Delimiter (Default is ';')")
parser.add_argument("--same_file", help = "Allows to overwrite the original file (True or False)")
parser.add_argument("--change_delimiter_only", help = "Option to only change the delimiter (True or False)")
args = parser.parse_args()

if (args.delimiter != None):
    final_delimiter = args.delimiter

if (check_csv(args.file) == True):
    #Test de si le fichier et les arguments sont bons
    if(args.change_delimiter_only != 'True'):
        #Si on souhaite convertir le fichier
        datain = open_csv(args.file, initial_delimiter)
        #On change chaque donnée
        data_out = []
        first_row = True
        for row in datain:
            if first_row == True:
                data = row
                first_row = False
            else:
                data = split_column(row, pattern_convertion)
            data = reorder_csv(data, pattern)
            data_out.append(data)

        #Renommage du fichier
        if(args.output != 'True'):
            if(args.same_file != 'True'):
                path_output = "file.csv"
            else:
                path_output = args.file
        else:
            path_output = args.output
        #écriture du résultat
        write_csv(path_output, data_out, final_delimiter)
    else:
        #seulement le délimiteur?
        datain = open(args.file)
        data_out = []
        for row in datain:
            data_out.append(replace_delimiter(row, initial_delimiter, final_delimiter))
        file_write = open(args.file, "w")
        for row in data_out:
            file_write.write(row)
else:
    #Message d'erreur
    print("File doesn't match requirements.")